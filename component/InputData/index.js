import React from 'react'
import { TextInput, Text, StyleSheet } from 'react-native'

const InputData = ({
    label, 
    placeholder,
    onChangeText,
    value,
    namaState}) => {

    return (
        <>
            <Text style={styles.title}>{label}</Text>
            <TextInput 
                style={styles.textInput} 
                placeholder={placeholder}
                onChangeText={(text) => onChangeText(namaState, text)}
                value={value}
                namaState="latlong" />
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50
    },
    title: {
        marginTop: 50,
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold'
    },
    textInput: {
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray'
    },
    Btn: {
        marginTop: 20,
        alignItems: 'center',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        backgroundColor: 'black'
    },
    textBtn: {
        color: 'white',
        fontSize: 14
    }
});

export default InputData
