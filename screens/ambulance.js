import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Text } from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { coordinates } from '../data/koordinat';
import { styles } from '../styles/styles';
import {distanceConversion, getDistance, getPreciseDistance} from 'geolib';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const Telkom_University = {latitude: -6.97210152702763, longitude: 107.6335199509465};
const RSU_Bina_Sehat = {latitude: -6.985840977655017, longitude: 107.62477902533556};

const GOOGLE_MAPS_APIKEY = 'AIzaSyBbeyQJj2pwI0jMl0wSPLNGKT8EfKURmp4';

const node = []
const marker = []

node_heuristic = getPreciseDistance(Telkom_University, RSU_Bina_Sehat) + (getPreciseDistance(Telkom_University, RSU_Bina_Sehat) * 0.2)

for(let i = 0; i < coordinates.length; i ++){
    if(getPreciseDistance(coordinates[i], Telkom_University) <= node_heuristic 
    && getPreciseDistance(coordinates[i], RSU_Bina_Sehat) <= node_heuristic){
        node.push(coordinates[i])
        //console.log(i)
        //console.log(coordinates[i])
        marker.push(
            <MapView.Marker key={i} coordinate={coordinates[i]} >
                <View style={{backgroundColor: '#0496ff'}} >
                    <Text> {i} </Text>
                </View>
            </MapView.Marker>
        )
    }
}

class Ambulance extends Component {
    constructor() {
        super()
        this.state = {
            initialPosition: {
            latitude: 0,
            longitude: 0,
            latitudeDelta: 0,
            longitudeDelta: 0,
            duration: 0,
            distance: 0
            },
        }
    }

    getTime = async (lat1, lng1, lat2, lng2) => {
        let URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+lat1+','+lng1+'&destinations='+lat2+'%2C'+lng2+'&departure_time=now&key=' + GOOGLE_MAPS_APIKEY;

        try {
            const response = await fetch(URL)
            const res = await response.json()
            this.setState({duration: res.rows[0].elements[0].duration_in_traffic.value})
        } catch (error) {
            console.error(error)
        }
    } 

    getActualDistance = async (lat1, lng1, lat2, lng2) => {
        let URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+lat1+','+lng1+'&destinations='+lat2+'%2C'+lng2+'&key=' + GOOGLE_MAPS_APIKEY;

        try {
            const response = await fetch(URL)
            const res = await response.json()
            this.setState({distance: res.rows[0].elements[0].distance.value})
        } catch (error) {
            console.error(error)
        }     
    }
    
    componentDidMount() {
        navigator.geolocation.getCurrentPosition((position) => {
            var lat = parseFloat(position.coords.latitude)
            var long = parseFloat(position.coords.longitude)
    
            var initialRegion = {
            latitude: lat,
            longitude: long,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        }
    
        this.setState({initialPosition: initialRegion})
        },
        (error) => alert(JSON.stringify(error)));
    }

    
    renderScreen = () => {
        return (
            <View style={styles.container}>
                <MapView
                style={styles.map}
                initialRegion={this.state.initialPosition}
                showsTraffic={true}>
                    { marker }  
                </MapView>
            </View>
        );
    }
    
    render() {
        return (
            this.renderScreen()
        );
    }
}

export default Ambulance;