import MapView, {Marker, UrlTile} from 'react-native-maps';
import * as React from 'react';
import FIREBASE from '../config/FIREBASE';
import axios from 'axios';

import {
    View, 
    Text, 
    TextInput, 
    Image,
    Linking,
    Button,
    Dimensions
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {CommonActions, useNavigation} from '@react-navigation/native';
import MapViewDirections from 'react-native-maps-directions';
import {distanceConversion, getDistance, getPreciseDistance} from 'geolib';

import {styles} from './../styles/styles';
import {coordinates} from './../data/koordinat';

const {width, height} = Dimensions.get('window')

const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
navigator.geolocation = require('@react-native-community/geolocation')

const Telkom_University = {latitude: -6.97210152702763, longitude: 107.6335199509465}
const RSU_Bina_Sehat = {latitude: -6.985840977655017, longitude: 107.62477902533556}
const Polsek_Bojongsoang = {latitude: -6.981324796791825, longitude: 107.63636734468342}
const Polrestabes_Bandung = {latitude: -6.91391933114792, longitude: 107.61050025478556}
const RSUD_Ali_Ihsan_Bandung = {latitude: -7.007741026496179, longitude: 107.6228539817707}
const Damkar_Kota_Bandung = {latitude: -6.939511490719972, longitude: 107.67245628362157}
const RSU_Pindad = {latitude: -6.937750845355122, longitude: 107.6463946978692}
const Klinik_Pratama = {latitude: -6.976014880367486, longitude: 107.63057956448485}
const Polisi_Buah_Batu = {latitude: -6.9585212559195835, longitude: 107.66612663413733}
const Polsekta_Buah_Batu = {latitude: -6.9411859933856634, longitude: 107.62752108084675}
const RS_SARTIKA_SARI_ASIH = {latitude: -6.956435224869796, longitude: 107.61230148068346}
const RS_Muhammadiyah = {latitude: -6.933302533699649, longitude: 107.62320969784741}
const RS_Edelweiss = {latitude: -6.943336075539749, longitude: 107.64964142780906}
const RSIA_Bandung = {latitude: -6.938885675101045, longitude: 107.66915786121412}
const RS_Immanuel = {latitude: -6.935405897526335, longitude: 107.59579024486986}
const RSIA_Khusus = {latitude: -6.943090645980279, longitude: 107.5914020928606}
const RS_Santosa = {latitude: -6.952408642577875, longitude: 107.58618338980756}
const RS_Bersalin_Ratmidjah = {latitude: -6.9448263473423335, longitude: 107.58212958143149}
const RS_Advent_Bandung = {latitude: -6.888955733812978, longitude: 107.60325429943758}
const RS_Pelita_Bunda = {latitude: -6.9588507372054975, loongitude: 107.63950719156998}

const GOOGLE_MAPS_APIKEY = "AIzaSyBbeyQJj2pwI0jMl0wSPLNGKT8EfKURmp4"

//maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=-6.97210152702763,107.6335199509465&destinations=-6.985840977655017%2C107.62477902533556&departure_time=now&key=AIzaSyBbeyQJj2pwI0jMl0wSPLNGKT8EfKURmp4

let node_heuristic = getPreciseDistance(Telkom_University, RSU_Bina_Sehat) + (getPreciseDistance(Telkom_University, RSU_Bina_Sehat) * 0.2)

//console.log(durasi_node)
//console.log(" Time")

// Algoritma A*
/* for(let a = 0; a < node.length-1; a++){
    for(let i = 0; i < node.length-1; i++){
        if((getDistance(current, node[i]) + getPreciseDistance(node[i], final)) <
            (getDistance(current, node[i+1]) + getPreciseDistance(node[i+1], final))){
                if(is_visited.length == 0){
                    next_node = node[i];
                }
                else {
                    for(let j = 0; j < is_visited.length; j++){
                        if(is_visited[j] != node[i]){
                            next_node = node[i];
                        }
                    }
                }
            }
            else {
                if(is_visited.length == 0){
                    next_node = node[i+1];
                }
                else {
                    for(let j = 0; j < is_visited.length; j++){
                        if(is_visited[j] != node[i+1]){
                            next_node = node[i+1];
                        }
                    }
                }
            }
        }

    direction.push(
            <MapViewDirections
                origin={current}
                destination={next_node}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3} 
                strokeColor="#0496ff"
            />
    );
    is_visited.push(current);
    current = next_node;
    
} */

export default class MapsScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            initialPosition: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0,
            },

            latlong: {},
            latlongkey: [],
            duration: 0,
            curr_duration: 0,
            distance: 0,
            curr_distance: 0,
            dist_neigh: 0,
            data: [],
            mark: [],
            algorithm: [],
            isLoading: false,
            algo: [],
            greed: [],
            asta: [],
            prim: [],
            hc: []
        }
    }

    getTime = async (lat1, lng1, lat2, lng2) => {
        let URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+lat1+','+lng1+'&destinations='+lat2+'%2C'+lng2+'&departure_time=now&key=' + GOOGLE_MAPS_APIKEY;

        try {
            const response = await fetch(URL)
            const res = await response.json()
            this.setState({duration: res.rows[0].elements[0].duration_in_traffic.value})
        } catch (error) {
            console.error(error)
        }
    } 

    getActualDistance = async (lat1, lng1, lat2, lng2) => {
        let URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+lat1+','+lng1+'&destinations='+lat2+'%2C'+lng2+'&key=' + GOOGLE_MAPS_APIKEY;

        try { 
            const response = await fetch(URL)
            const res = await response.json()
            this.setState({distance: res.rows[0].elements[0].distance.value})
        } catch (error) {
            console.error(error)
        }     
    }

    componentDidMount() {
        /*FIREBASE.database()
            .ref('Latlong')
            .once('value', (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {}
                let latItem = {...data}

                this.setState({
                    latlong: latItem,
                    latlongkey: Object.keys(latItem)
                })
            })*/

        navigator.geolocation.getCurrentPosition((position) => {
            var lat = parseFloat(position.coords.latitude)
            var long = parseFloat(position.coords.longitude)
    
            var initialRegion = {
                latitude: lat,
                longitude: long,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
        }
    
        this.setState({initialPosition: initialRegion})
        },
        (error) => alert(JSON.stringify(error)));
    }

    AStar2 = async () => {
        let next_node = {}
        let node = []
        let mark = []
        let key = 0
        let status = true
        let node_neigh = []
        let index = 0
        let direction = []
        let neigh = []
        let distance_to_current = 0
        let current = Telkom_University
        let destination = RSU_Bina_Sehat
        let close_one = {}
        let test = 0
        mark.push(
            <MapView.Marker key={212223321} coordinate={current} />,
            <MapView.Marker key={212223091} coordinate={destination} />
            
        )
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.2)
        //this.getTime(Telkom_University.latitude, Telkom_University.longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }

        node.push(destination)
        
        while(status){
            neigh = this.neighbour(current, node)
            for(let i = 0; i < neigh.length; i++){
                await this.getTime(current.latitude, current.longitude, neigh[i].latitude, neigh[i].longitude)
                await this.getActualDistance(current.latitude, current.longitude, neigh[i].latitude, neigh[i].longitude)
                console.log(neigh)
                console.log(current)
                console.log(next_node)
                if(i == 0){
                    this.setState({curr_duration: this.state.duration})
                    this.setState({curr_distance: this.state.distance})
                    if(current == next_node){
                        next_node = {latitude: -6.937571228777847, longitude: 107.6093034794365}
                    } else {
                        next_node = neigh[i]
                    }
                    distance_to_current = getPreciseDistance(destination, next_node) + this.state.curr_distance
                } else {
                    if(((distance_to_current * 0.5) + (this.state.curr_duration * 0.5)) > (((getPreciseDistance(neigh[i], destination) + this.state.distance) * 0.5) + (this.state.duration * 0.5))){
                        test += 1
                        this.setState({curr_duration: this.state.duration})
                        this.setState({curr_distance: this.state.distance})
                        next_node = neigh[i]
                        distance_to_current = getPreciseDistance(destination, next_node) + this.state.curr_distance
                        for(let k = 0; k < node.length; k++){
                            if(next_node == node[k]){
                                index = k
                            }
                        }
                    }
                }

                if(i == neigh.length-1){
                    direction.push(
                        <MapViewDirections
                            key={key}
                            origin={current}
                            destination={next_node}
                            strokeWidth={3}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeColor="#0496ff"
                            mode="DRIVING"
                        />
                    )
                    
                    if(next_node == destination){
                        status = false
                    }
                    current = next_node
                    node.splice(index, 1)
                }
            }
            key += 1
        }
        
        console.log(direction)
        console.log("A* Done!")
        this.setState({mark: mark})
        this.setState({asta: direction})
    }

    Prim = async () => {
        let current = []
        let neigh = []
        let mark = []
        let direction = []
        let key = 0
        let stat = false
        current.push(Telkom_University)
        let destination = Polsek_Bojongsoang
        let next_node = {}
        let current_now = {}
        let index = 0
        let status = true
        let node = []
        let heuristic = getPreciseDistance(current[0], destination) //+ (getPreciseDistance(current[0], destination) * 0.2)
        mark.push(
            <MapView.Marker key={212223321} coordinate={current[0]} />,
            <MapView.Marker key={212223091} coordinate={destination} />    
        )
        
        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], Telkom_University) <= heuristic 
            && getPreciseDistance(coordinates[i], RSU_Bina_Sehat) <= heuristic){
                node.push(coordinates[i])
            }
        }

        node.push(destination)

        this.setState({curr_distance: 10000})
        this.setState({curr_duration: 10000})
        current_now = Telkom_University
        
        while(status){
            for(let i = 0; i < current.length; i++){
                neigh = this.neighbour(current[i], node)
                for(let j = 0; j < neigh.length; j++){
                    console.log(neigh)
                    console.log(next_node)
                    await this.getActualDistance(current[i].latitude, current[i].longitude, neigh[j].latitude, neigh[j].longitude)
                    await this.getTime(current[i].latitude, current[i].longitude, neigh[j].latitude, neigh[j].longitude)
                    if((this.state.curr_distance) > (this.state.distance)){
                        if(current[j] == next_node){
                            next_node = neigh[0]
                        } else {
                            next_node = neigh[j]
                        }
                        current_now = current[i]
                        this.setState({curr_distance: this.state.distance})
                        this.setState({curr_duration: this.state.duration})
                    }
                    for(let z = 0; z < node.length; z++){
                        if(node[z] == next_node){
                            index = z
                        }
                    }
                    
                }
            }
            //if(i == current.length-1){
                if(next_node == destination){
                    status = false
                }
                
                direction.push(
                    <MapViewDirections
                        key={key}
                        origin={current_now}
                        destination={next_node}
                        strokeWidth={3}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeColor="#0496ff"
                        mode="WALKING"
                    />
                )
    
                /*for(let g = 0; g < current.length; g++){
                    if(current[g] != next_node){
                        if(next_node != null){
                            stat = true
                        }
                    }
                }*/
    
                //if(stat){
                    current.push(next_node)
                //}
                this.setState({curr_distance: 10000})
                stat = false
                node.splice(index, 1)
                key += 1
            //}
        }

        console.log("Done!")
        console.log(direction)
        this.setState({mark: mark})
        this.setState({prim: direction})
    }

    Hill_climbing = async () => {
        let next_node = {}
        let node = []
        let random = 0
        let mark = []
        let key = 0
        let index = 0
        let direction = []
        let neigh = []
        let test = 0
        let distance_to_current = 0
        let current = Telkom_University
        let destination = Polsek_Bojongsoang
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.2)
        mark.push(
            <MapView.Marker key={212223321} coordinate={current} />
        )
        mark.push(
            <MapView.Marker key={212223091} coordinate={destination} />
        )

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }

        node.push(destination)

        while(current != destination){
            neigh = this.neighbour(current, node);
            for(let i = 0; i < neigh.length; i++){
                await this.getTime(current.latitude, current.longitude, neigh[i].latitude, neigh[i].longitude)
                if(i == 0){
                    this.setState({curr_duration: this.state.duration})     
                    next_node = neigh[i]
                    distance_to_current = getPreciseDistance(destination, next_node)
                } else {
                    if(((distance_to_current * 0.5) + (this.state.curr_duration * 0.5)) > ((getPreciseDistance(neigh[i], destination) * 0.5) + (this.state.duration * 0.5))){
                        next_node = neigh[i]
                        this.setState({curr_duration: this.state.duration})
                        distance_to_current = getPreciseDistance(destination, next_node)
                        for(let k = 0; k < node.length; k++){
                            if(next_node == node[k]){
                                index = k
                            }
                        }
                    }
                }
                
                if(i == neigh.length-1){
                    direction.push(
                        <MapViewDirections
                            key={key}
                            origin={current}
                            destination={next_node}
                            strokeWidth={3}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeColor="#0496ff"
                            mode="DRIVING"
                        />
                    )
                    mark.push(
                        <MapView.Marker key={key} coordinate={next_node} />
                    )
                    current = next_node
                    node.splice(index, 1)
                }
            }
            key =+ 1

        }        
        console.log("Hill Done!")
        this.setState({mark: mark})
        this.setState({hc: direction})
    }

    Greedy = async () => {
        let next_node = {}
        let node = []
        let random = 0
        let mark = []
        let key = 0
        let index = 0
        let direction = []
        let neigh = []
        let test = 0
        let distance_to_current = 0
        let current = Telkom_University
        let destination = Polsek_Bojongsoang
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.2)
        mark.push(
            <MapView.Marker key={212223321} coordinate={current} />
        )
        mark.push(
            <MapView.Marker key={212223091} coordinate={destination} />
        )
        //this.getTime(Telkom_University.latitude, Telkom_University.longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }

        node.push(destination)
        
        while(current != destination){
            neigh = this.neighbour(current, node)
            for(let i = 0; i < neigh.length; i++){
                await this.getTime(current.latitude, current.longitude, neigh[i].latitude, neigh[i].longitude)
                if(i == 0){
                    this.setState({curr_duration: this.state.duration})     
                    next_node = neigh[i]
                    distance_to_current = getPreciseDistance(destination, next_node)
                } else {
                    if(((distance_to_current * 0.5) + (this.state.curr_duration * 0.5)) > ((getPreciseDistance(neigh[i], destination) * 0.5) + (this.state.duration * 0.5))){
                        test += 1
                        next_node = neigh[i]
                        this.setState({curr_duration: this.state.duration})
                        distance_to_current = getPreciseDistance(destination, next_node)
                        for(let k = 0; k < node.length; k++){
                            if(next_node == node[k]){
                                index = k
                            }
                        }
                    }
                }

                if(i == neigh.length-1){
                    direction.push(
                        <MapViewDirections
                            key={key}
                            origin={current}
                            destination={next_node}
                            strokeWidth={3}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeColor="#0496ff"
                            mode="DRIVING"
                        />
                    )
                    /*mark.push(
                        <MapView.Marker key={key} coordinate={next_node} />
                    )*/
                    current = next_node
                    node.splice(index, 1)
                }
            }
            key += 1
        }
        //console.log(direction)
        //console.log(test)
        console.log("Greedy Done!")
        this.setState({mark: mark})
        this.setState({greed: direction})
        //this.setState({mark: mark})
    }

    neighbour(current, neigh){
        let n = []
        let close_one = {}
        let index = 0
        let visited = []
        close_one = neigh[0]
        let status = false
        let close_dist = getDistance(current, close_one)

        /*for(let j = 0; j < 4; j++){
            //if(close_one != current){
                for(let i = 0; i < neigh.length; i++){
                    if(i == 0){
                        close_one = neigh[i]
                        index = i
                        close_dist = getDistance(current, neigh[i])
                    } else {
                        if(close_dist > getDistance(current, neigh[i])){
                            close_one = neigh[i]
                            index = i
                            close_dist = getDistance(current, neigh[i])
                        }
                    }
                }
            /*} else {
                close_one = neigh[1]
                for(let i = 0; i < neigh.length; i++){
                    if(i == 0){
                        close_one = neigh[i]
                        index = i
                        close_dist = getDistance(current, neigh[i])
                    } else {
                        if(close_dist > getDistance(current, neigh[i])){
                            close_one = neigh[i]
                            index = i
                            close_dist = getDistance(current, neigh[i])
                        }
                    }
                } 
            } 
            n.push(close_one);
            neigh.splice(index, 1);
            //visited.push(close_one)
            close_one = neigh[0];
        } */

        for(let j = 0; j < 4; j++){
            for(let i = 0; i < neigh.length; i++){
                if(visited.length > 0){
                    for(let a = 0; a < visited.length; a++){
                        if(close_one == neigh[i]){
                            status = true
                            if(close_one != visited[a] && a == visited.length-1){
                                if(getDistance(close_one, current) > getDistance(neigh[i], current)){
                                    close_one = neigh[i]
                                    index = i;
                                }
                            } else {
                                if(i == neigh.length - 1){
                                    close_one = neigh[0]
                                    index = 0
                                } else {
                                    close_one = neigh[i+1]
                                    index = i+1
                                }
                            }
                        }
                    }
                } else {
                    if(close_one != neigh[i]){
                        if(getDistance(close_one, current) > getDistance(neigh[i], current)){
                            close_one = neigh[i];
                            index = i;
                        }
                    }
                }
            }
            n.push(close_one);
            //neigh.splice(index, 1);
            visited.push(close_one)
            //close_one = neigh[0];
        }

        return n;
    }

    neighbour2(){
        let n = []
        let close_one = {}
        let index = 0
        let visited = []
        close_one = neigh[0]
        let close_dist = getDistance(current, close_one)

        for(let j = 0; j < 4; j++){
            if(close_one != current){
                for(let i = 0; i < neigh.length; i++){
                    if(i == 0){
                        close_one = neigh[i]
                        index = i
                        close_dist = getDistance(current, neigh[i])
                    } else {
                        if(close_dist > getDistance(current, neigh[i])){
                            close_one = neigh[i]
                            index = i
                            close_dist = getDistance(current, neigh[i])
                        }
                    }
                }
            } else { 
                close_one = neigh[1]
                for(let i = 0; i < neigh.length; i++){
                    if(i == 0){
                        close_one = neigh[i]
                        index = i
                        close_dist = getDistance(current, neigh[i])
                    } else {
                        if(close_dist > getDistance(current, neigh[i])){
                            close_one = neigh[i]
                            index = i
                            close_dist = getDistance(current, neigh[i])
                        }
                    }
                } 
            } 
            n.push(close_one);
            neigh.splice(index, 1);
            //visited.push(close_one)
            close_one = neigh[0];
        }

        /*for(let j = 0; j < 4; j++){
            for(let i = 0; i < neigh.length; i++){
                if(visited.length > 0){
                    for(let a = 0; a < visited.length; a++){
                        if(close_one != neigh[i]){
                            if(close_one != visited[a] && a == visited.length-1){
                                if(getDistance(close_one, current) > getDistance(neigh[i], current)){
                                    close_one = neigh[i];
                                    index = i;
                                }
                            } else {
                                if(i == neigh.length - 1){
                                    close_one = neigh[0]
                                    index = 0
                                } else {
                                    close_one = neigh[i+1]
                                    index = i+1
                                }
                            }
                        }
                    }
                } else {
                    if(close_one != neigh[i]){
                        if(getDistance(close_one, current) > getDistance(neigh[i], current)){
                            close_one = neigh[i];
                            index = i;
                        }
                    }
                }
            }
            n.push(close_one);
            //neigh.splice(index, 1);
            visited.push(close_one)
            //close_one = neigh[0];
        }*/

        return n;
    }

    perhitungan () {
        let node = []
        let mark = []
        let current = Telkom_University
        let k  = 0
        let neigh = []
        let destination = RSU_Bina_Sehat
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.1)
        //node.push(current)

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
                console.log(coordinates[i])
                mark.push(
                    <MapView.Marker key={k} coordinate={coordinates[i]} />
                ) 
                k += 1
            }
        }
        //node.push(destination)
        //console.log(node)

        /*for(let a = 0; a < node.length-1; a++){
            neigh = this.neighbour(node[a], node)
            console.log(node[a], " Neighbour: ")
            console.log(neigh)
            console.log("")
            
        }*/

        this.setState({mark: mark})
        
    }

    marknode() {
        let node = []
        let mark = []
        let point = {}
        let current = Telkom_University
        let k  = 0
        let neigh = []
        let destination = RSU_Bina_Sehat
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.1)
        //node.push(current)

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }
        point =  {latitude: -6.985556911981533, longitude: 107.63286096207099}
        neigh = this.neighbour(point, node)
        console.log(neigh)
    }

    hitungheuristik(){
        let node = {"latitude": -6.972155430431573, "longitude": 107.63355639562371} 
        let next = {latitude: -6.982389880837143, longitude: 107.63370000642463}
        console.log(getDistance(node, RSU_Bina_Sehat))
    }

    hitungactual = async() => {
        let node = []
        let mark = []
        let point = {}
        let current = Telkom_University
        let next = {}
        let k  = 0
        let neigh = []
        let destination = RSU_Bina_Sehat
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.1)
        //node.push(current)

        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }

        let arr_point = [
            {"latitude": -6.972155430431573, "longitude": 107.63355639562371}, //0 1
            {"latitude": -6.9725440852050955, "longitude": 107.63404130699072}, //1 1
            {"latitude": -6.972822566764564, "longitude": 107.6361690705928}, // 2 2
            {"latitude": -6.973772407939741, "longitude": 107.63589311523947}, // 3 29
            {"latitude": -6.9807495695527235, "longitude": 107.63414118881632}, // 4 30
            {"latitude": -6.982389880837143, "longitude": 107.63370000642463}, // 5 31
            {"latitude": -6.984567710991031, "longitude": 107.62904924075819}, // 6 32
            {"latitude": -6.986243715132659, "longitude": 107.62646520917428}, // 7 33
            {"latitude": -6.973640992026127, "longitude": 107.61792846954697}, // 8 34
            {"latitude": -6.973585333671192, "longitude": 107.62751544176167}, // 9 61
            {"latitude": -6.972839876265336, "longitude": 107.62510871486984}, // 10 62
            {"latitude": -6.972912013560205, "longitude": 107.6217777839639}, // 11 63
            {"latitude": -6.973656789623069, "longitude": 107.61795967631137}, // 12 64
            {"latitude": -6.973990611615119, "longitude": 107.63259963710726}, // 13 72
            {"latitude": -6.989315268557589, "longitude": 107.63229673296148}, // 14 510
            {"latitude": -6.975259516897302, "longitude": 107.61849396600336}, // 15 828
            {"latitude": -6.9775859584130595, "longitude": 107.61930531146736}, // 16 829
            {"latitude": -6.983799437894412, "longitude": 107.62155620302354}, // 17 830
            {"latitude": -6.97961323715476, "longitude": 107.6292256091285}, // 18 831
            {"latitude": -6.978350384684502, "longitude": 107.63226033339484}, // 19 832
            {"latitude": -6.979609121885959, "longitude": 107.63108519675917}, // 20 833
            {"latitude": -6.9818664487472315, "longitude": 107.62904772969645}, // 21 834
            {"latitude": -6.973308449600742, "longitude": 107.63340408741792}, // 22 835
        ]

        /*wait this.getTime(arr_point[0].latitude, arr_point[0].longitude, arr_point[1].latitude, arr_point[1].longitude) // 1
        console.log(this.state.duration)
        await this.getTime(arr_point[0].latitude, arr_point[0].longitude, arr_point[22].latitude, arr_point[22].longitude) // 2
        console.log(this.state.duration)
        await this.getTime(arr_point[1].latitude, arr_point[1].longitude, arr_point[22].latitude, arr_point[22].longitude) // 2
        console.log(this.state.duration)
        await this.getTime(arr_point[1].latitude, arr_point[1].longitude, arr_point[2].latitude, arr_point[2].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[22].latitude, arr_point[22].longitude, arr_point[13].latitude, arr_point[13].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[13].latitude, arr_point[13].longitude, arr_point[9].latitude, arr_point[9].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[13].latitude, arr_point[13].longitude, arr_point[19].latitude, arr_point[19].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[2].latitude, arr_point[2].longitude, arr_point[3].latitude, arr_point[3].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[9].latitude, arr_point[9].longitude, arr_point[10].latitude, arr_point[10].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[19].latitude, arr_point[19].longitude, arr_point[20].latitude, arr_point[20].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[3].latitude, arr_point[3].longitude, arr_point[4].latitude, arr_point[4].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[11].latitude, arr_point[11].longitude, arr_point[12].latitude, arr_point[12].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[20].latitude, arr_point[20].longitude, arr_point[18].latitude, arr_point[18].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[4].latitude, arr_point[4].longitude, arr_point[5].latitude, arr_point[5].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[12].latitude, arr_point[12].longitude, arr_point[15].latitude, arr_point[15].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[18].latitude, arr_point[18].longitude, arr_point[21].latitude, arr_point[21].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[5].latitude, arr_point[5].longitude, arr_point[6].latitude, arr_point[6].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[15].latitude, arr_point[15].longitude, arr_point[16].latitude, arr_point[16].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[21].latitude, arr_point[21].longitude, arr_point[6].latitude, arr_point[6].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[16].latitude, arr_point[16].longitude, arr_point[17].latitude, arr_point[17].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[6].latitude, arr_point[6].longitude, arr_point[7].latitude, arr_point[7].longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[17].latitude, arr_point[17].longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)
        console.log(this.state.duration)
        await this.getTime(arr_point[7].latitude, arr_point[7].longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)
        console.log(this.state.duration) */

        await this.getActualDistance(arr_point[0].latitude, arr_point[0].longitude, arr_point[1].latitude, arr_point[1].longitude) // 1
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[0].latitude, arr_point[0].longitude, arr_point[22].latitude, arr_point[22].longitude) // 2
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[1].latitude, arr_point[1].longitude, arr_point[22].latitude, arr_point[22].longitude) // 2
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[1].latitude, arr_point[1].longitude, arr_point[2].latitude, arr_point[2].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[22].latitude, arr_point[22].longitude, arr_point[13].latitude, arr_point[13].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[13].latitude, arr_point[13].longitude, arr_point[9].latitude, arr_point[9].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[13].latitude, arr_point[13].longitude, arr_point[19].latitude, arr_point[19].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[2].latitude, arr_point[2].longitude, arr_point[3].latitude, arr_point[3].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[9].latitude, arr_point[9].longitude, arr_point[10].latitude, arr_point[10].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[19].latitude, arr_point[19].longitude, arr_point[20].latitude, arr_point[20].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[3].latitude, arr_point[3].longitude, arr_point[4].latitude, arr_point[4].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[11].latitude, arr_point[11].longitude, arr_point[12].latitude, arr_point[12].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[20].latitude, arr_point[20].longitude, arr_point[18].latitude, arr_point[18].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[4].latitude, arr_point[4].longitude, arr_point[5].latitude, arr_point[5].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[12].latitude, arr_point[12].longitude, arr_point[15].latitude, arr_point[15].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[18].latitude, arr_point[18].longitude, arr_point[21].latitude, arr_point[21].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[5].latitude, arr_point[5].longitude, arr_point[6].latitude, arr_point[6].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[15].latitude, arr_point[15].longitude, arr_point[16].latitude, arr_point[16].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[21].latitude, arr_point[21].longitude, arr_point[6].latitude, arr_point[6].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[16].latitude, arr_point[16].longitude, arr_point[17].latitude, arr_point[17].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[6].latitude, arr_point[6].longitude, arr_point[7].latitude, arr_point[7].longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[17].latitude, arr_point[17].longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)
        console.log(this.state.distance)
        await this.getActualDistance(arr_point[7].latitude, arr_point[7].longitude, RSU_Bina_Sehat.latitude, RSU_Bina_Sehat.longitude)
        console.log(this.state.distance)

        point = {"latitude": -6.973772407939741, "longitude": 107.63589311523947}
        next = {"latitude": -6.972822566764564, "longitude": 107.6361690705928}
        
        /*await this.getActualDistance(point.latitude, point.longitude, next.latitude, next.longitude)
        console.log(this.state.distance)*/
    }

    showtime = async () => {
        let node = []
        let current = Telkom_University
        let destination = RSU_Bina_Sehat
        let heuristic = getPreciseDistance(current, destination) + (getPreciseDistance(current, destination) * 0.5)
        node.push(current)
        for(let i = 0; i < coordinates.length; i++){
            if(getPreciseDistance(coordinates[i], current) <= heuristic 
            && getPreciseDistance(coordinates[i], destination) <= heuristic){
                node.push(coordinates[i])
            }
        }

        for(let i = 0; i < node.length-1; i++){
            await this.getActualDistance(node[i].latitude, node[i].longitude, node[i+1].latitude, node[i+1].longitude)
            console.log(this.state.distance)
        }
    }

    openDialScreen = () => {
        let number = '';
        if (Platform.OS === 'ios') {
          number = 'telprompt:${+62227503482}';
        } else {
          number = 'tel:${+62227503482}';
        }
        Linking.openURL(number);
    }

    render() {
        return (
            <View style={styles.map}>
                <MapView
                style={styles.map}
                initialRegion={this.state.initialPosition}
                showsTraffic={false}
                >
                    {this.state.mark}
                    {
                        this.state.algorithm
                    }

                </MapView>

                <View style={{bottom: 0, top: 60, position: 'absolute'}}>
                <TouchableOpacity style={styles.proses} onPress={() => this.AStar2()}>
                        <Text style={{color: '#173248', textAlignVertical: 'center'}}>Proses A*</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.proses} onPress={() => this.Prim()}>
                        <Text style={{color: '#173248', textAlignVertical: 'center'}}>Proses Prim</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.proses} onPress={() => this.Greedy()}>
                        <Text style={{color: '#173248', textAlignVertical: 'center'}}>Proses Greedy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.proses} onPress={() => this.Hill_climbing()}>
                        <Text style={{color: '#173248', textAlignVertical: 'center'}}>Proses Hill Climbing</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginBottom: 50, marginTop: 350, marginLeft: 260}} 
                        onPress={() => this.openDialScreen()}>
                        <Image 
                            source={require('./../assets/accept_call.png')}></Image>
                        </TouchableOpacity>
                </View>
                
                <View style={{bottom: 0, top: 700, position: 'absolute'}}>
                <TouchableOpacity style={styles.start_algorithm} onPress={() => this.setState({algorithm: this.state.asta})}>
                        <Text style={{color: '#fff', textAlignVertical: 'center'}}>Algoritma A*</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.start_algorithm} onPress={() => this.setState({algorithm: this.state.prim})}>
                        <Text style={{color: '#fff', textAlignVertical: 'center'}}>Algoritma Prim</Text> 
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.start_algorithm} onPress={() => this.setState({algorithm: this.state.greed})}>
                        <Text style={{color: '#fff', textAlignVertical: 'center'}}>Algoritma Greedy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.start_algorithm} onPress={() => this.setState({algorithm: this.state.hc})}>
                        <Text style={{color: '#fff', textAlignVertical: 'center'}}>Algoritma Hill Climbing</Text> 
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
    
}