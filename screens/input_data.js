import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native'
import InputData from '../component/InputData';
import FIREBASE from '../config/FIREBASE'
import { coordinates } from '../data/koordinat';

export default class input_data extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             latlong: ''
        }
    }
    
    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value
        })
    }

    onSubmit = () => {
        if(this.state.latlong){
            const LatlongReferensi = FIREBASE.database().ref('Latlong')
            const Latlong = {
                Latlong: this.state.latlong
            }

            /*for(let i = 0; i < coordinates.length; i++){
                LatlongReferensi.
                push(coordinates[i])
                .then((data) => {
                    //Alert.alert('Sukses','Berhasil menambahkan Latlong')
                    //this.props.navigation.replace('Home')
                })
                .catch((error) => {
                    console.log('Error: ', error)
                }) 
            }*/

            /*LatlongReferensi.
                push(Latlong)
                .then((data) => {
                    Alert.alert('Sukses','Berhasil menambahkan Latlong')
                    this.props.navigation.replace('Home')
                })
                .catch((error) => {
                    console.log('Error: ', error)
                }) */

        }
        else {
            Alert.alert('Error', 'Isi kolom terlebih dahulu')
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <InputData 
                    label="latlong" 
                    placeholder="Masukan LatLong"
                    onChangeText={this.onChangeText}
                    value={this.state.latlong}
                    namaState="latlong" />

                <TouchableOpacity style={styles.Btn} onPress={() => this.onSubmit()}>
                    <Text style={styles.textBtn}>Submit</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 50
    },
    title: {
        marginTop: 50,
        marginBottom: 10,
        fontSize: 15,
        fontWeight: 'bold'
    },
    textInput: {
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray'
    },
    Btn: {
        marginTop: 20,
        alignItems: 'center',
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        backgroundColor: 'black'
    },
    textBtn: {
        color: 'white',
        fontSize: 14
    }
});
